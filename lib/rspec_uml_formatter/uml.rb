# Not thread safe yet.
module RspecUmlFormatter
  module Uml

    extend self
    extend Forwardable

    autoload :ForkNode, 'rspec_uml_formatter/uml/fork_node'
    autoload :ConditionNode, 'rspec_uml_formatter/uml/condition_node'
    autoload :ActivityNode, 'rspec_uml_formatter/uml/activity_node'
    autoload :Diagram, 'rspec_uml_formatter/uml/diagram'

    INITIATOR = "@startuml"
    TERMINATOR = "@enduml"
    # START = "start"
    # NEW_PAGE = "newpage"
    TITLE = "title"
    # END_PAGE = "end"
    # DETACH = "detach"

    attr_reader :diagrams, :persisters

    def_instance_delegators :current_diagram, :add_fork, :add_condition, :add_group, :add_activity, :pop

    def current_diagram
      diagrams.last
    end

    def start(name, options = {})
      diagram = Diagram.new(name, options)
      diagrams.push(diagram)
      diagram
    end

    def stop(options = {})
      diagram = diagrams.last
      diagram&.stop(options)
      diagram
    ensure
      diagrams.delete(diagram)
    end

    def persisters
      @persisters
    end

    def line_output(text)
      text + "\n"
    end

    def start_output
      line_output(INITIATOR)
    end

    def end_output
      line_output(TERMINATOR)
    end

    def title_output(title)
      line_output("#{TITLE} #{title}")
    end

    def vertical_layout
      line_output("!pragma useVerticalIf on")
    end

    private

    def initialize_ivars
      @persisters = RspecUmlFormatter::Persisters.new
      @diagrams = []
    end

    initialize_ivars # to avoid warnings

    # def end_page
    #   line(END_PAGE)
    # end
  end
end