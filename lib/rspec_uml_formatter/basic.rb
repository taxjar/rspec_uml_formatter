RSpec::Support.require_rspec_core "formatters/base_formatter"
require 'rspec_uml_formatter'

module RspecUmlFormatter
  class Basic < RSpec::Core::Formatters::BaseFormatter
    RSpec::Core::Formatters.register(
      self,
      :start,
      :message,
      :example_group_started,
      :example_started,
      :example_group_finished,
      :close
    )

    def initialize(output)
      super
      dir = ENV.fetch('UML_DIR', 'docs/uml')
      @output.puts "Placing UML docs in: #{dir}"
      Uml.persisters[:file_system].storage_location = dir
      @group_level = 0
      @output.puts "Starting"
    end

    def start(_notification)
      # output.puts "Starting suite"
      # TODO: split large files, scale? See PlantUml PDF
      # Uml.use_diagram()
    end

    def example_group_started(notification)
      # TODO: write to separate files (configurable?)
      # described_class would be title of file

      output.puts "#{current_indentation}#{notification.group.description.strip}"

      # TOOD: use metadata to define conditions
      # metadata - :description_args=>["context"]

      # TOOD: add notes for this?
      # https://www.rubydoc.info/gems/rspec-core/RSpec/Core/ExampleGroup#currently_executing_a_context_hook%3F-class_method
      # Can this be used for something? notification.currently_executing_a_context_hook? ⇒ Boolean
      # Returns true if a before(:context) or after(:context) hook is currently executing.

      label = notification.group.description.strip

      if @group_level == 0
        Uml.start file_name(notification.group.metadata), layout: layout(notification.group.metadata)
        Uml.add_fork label: label, parent: nil
      else
        if condition?(notification)
          Uml.add_condition(label)
        else
          Uml.add_group(label)
          Uml.start file_name(notification.group.metadata), layout: layout(notification.group.metadata)
          Uml.add_fork label: label, parent: nil
        end
      end

      @group_level += 1
    end

    def example_started(notification)
      output.puts "#{current_indentation}#{notification.example.description.strip}"

      Uml.add_activity notification.example.description.strip
    end

    def example_group_finished(notification)
      @group_level -= 1 if @group_level > 0

      # output.puts "#{current_indentation}finished #{notification.group.description.strip}"
      # @current_node = @current_node.parent unless @current_node.parent.nil?

      # output.puts @current_node.inspect
      # if @group_level == 0
        # @doc += @current_node.to_s unless @current_node.nil?
      # end
      if condition?(notification)
        Uml.pop if Uml.current_diagram
      else
        Uml.stop
      end
    end

    def close(_notification)
      # output.puts "FIN"
      # output.puts @current_node.inspect
      # @doc += RspecUmlFormatter::Uml.end
      # output.puts @doc
    end

    def message(notification)
      # output.puts "Custom message: #{notification.message}"
    end

    # from https://github.com/vcr/vcr/blob/master/lib/vcr/test_frameworks/rspec.rb
    def file_name(metadata)
      description = if metadata[:description].empty?
                      # we have an "it { is_expected.to be something }" block
                      metadata[:scoped_id]
                    else
                      metadata[:description]
                    end
      example_group = if metadata.key?(:example_group)
                        metadata[:example_group]
                      else
                        metadata[:parent_example_group]
                      end

      if example_group
        [file_name(example_group), description].join('/')
      else
        description
      end
    end

    def layout(metadata)
      metadata.dig(:uml, :layout) if metadata[:uml].is_a? Hash
    end

    def condition?(notification)
      return notification.group.metadata.dig(:uml, :condition) if notification.group.metadata[:uml].is_a? Hash
    end

    private

    def current_indentation
      '  ' * @group_level
    end
  end
end
