module RspecUmlFormatter
  module Uml
    class Diagram

      attr_reader :name, :options

      def initialize(name, options = {})
        @name = name
        @persister = Uml.persisters[options.fetch(:persister, :file_system)]
        @options = options
        @output = Uml.start_output()
        @output += Uml.vertical_layout if layout == :vertical
        @output += Uml.title_output(@name)
      end

      def add_fork(label:, parent:)
        @current_node = ForkNode.new(label: label, parent: parent)
      end

      def add_condition(label)
        node = @current_node.add_condition(label)
        @current_node = node
      end

      def add_group(label)
        node = @current_node.add_group(label)
        # @current_node = node
      end

      def add_activity(label)
        @current_node.add_activity(label)
      end

      def stop(options)
        @output += @current_node.to_s
        @output += Uml.end_output()
        write_uml_to_disk
      end

      def output
        @output ||= ""
      end

      def pop
        @current_node = @current_node&.parent
      end

      def layout
        @_layout ||= options[:layout]
      end

      private

      def storage_key
        @storage_key ||= "#{name}.txt"
      end

      def write_uml_to_disk
        return if output.empty?
        @persister[storage_key] = @output
      end

    end
  end
end
