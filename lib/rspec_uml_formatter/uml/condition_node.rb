module RspecUmlFormatter
  module Uml
    class ConditionNode < ForkNode
      INITIATOR = 'if'
      REPEATER = 'elseif'
      TERMINATOR = "else\nend\nendif"

      def initiator
        Uml.line_output("#{INITIATOR} (#{label}) then (yes)")
      end

      def repeater
        Uml.line_output("#{REPEATER} (#{label}) then (yes)")
      end

      def terminator
        ''
      end
    end
  end
end