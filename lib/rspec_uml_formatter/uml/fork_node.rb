module RspecUmlFormatter
  module Uml
    class ForkNode
      attr_reader :forks, :conditions, :activities, :label, :parent

      INITIATOR = 'split'
      REPEATER = 'split again'
      TERMINATOR = "end split"

      # POSITIONS = { start: 0, middle: 1, end: 2}
      # POSITION_PREFIX = { start: INITIATOR, middle: REPEATER, end: TERMINATOR }

      def initialize(label:, parent:)
        @label = label
        @forks = []
        @parent = parent
        @activities = []
        @conditions = []
      end

      def add_group(group)
        node = ForkNode.new(label: group, parent: self)
        @forks << node
        node
      end

      def add_condition(condition)
        node = ConditionNode.new(label: condition, parent: self)
        @conditions << node
        node
      end

      def add_activity(activity)
        node = ActivityNode.new(label: activity, parent: self)
        @forks << ActivityNode.new(label: activity, parent: self)
        node
      end

      def to_s
        @output = self.class == RspecUmlFormatter::Uml::ForkNode ? initiator : ''
        print_forks()
        print_conditions()

        if forks.empty?
          @output
        else
          @output += Uml.line_output(TERMINATOR)
        end

        @output += terminator if parent.nil? && self.class == RspecUmlFormatter::Uml::ForkNode

        @output
      end

      def initiator
        o = ''
        o += Uml.line_output("start") if parent.nil?
        o += Uml.line_output("partition #{label.gsub(/\s/, '_')} {")
      end

      def repeater
        # can override
      end

      def terminator
        o = Uml.line_output("}")
        o += Uml.line_output("end") if parent.nil?
        o
      end

      private

      def print_forks()
        forks.each_with_index do |f, i|
          @output += i == 0 ? Uml.line_output(INITIATOR) : Uml.line_output(REPEATER)
          @output += f.to_s
          @output += f.terminator
        end
      end

      def print_conditions()
        return if conditions.empty?

        @output += Uml.line_output(REPEATER) unless forks.empty?

        conditions.each_with_index do |c, i|
          @output += i == 0 ? c.initiator : c.repeater
          @output += c.to_s
        end

        @output += Uml.line_output(ConditionNode::TERMINATOR)
      end
    end
  end
end
