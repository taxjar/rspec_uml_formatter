module RspecUmlFormatter
  module Uml
    class ActivityNode
      attr_reader :label, :parent

      def initialize(label:, parent:)
        @label = label
        @parent = parent
      end

      def initiator
        ''
      end

      def terminator
        ''
      end

      def to_s
        # TODO: split up in multi lines if above some configurable/default
        Uml.line_output(":#{label};")
      end
    end
  end
end