require "rspec/core/rake_task"

namespace :ruf do
  task run: %w[spec_uml ruf:generate_images]

  # RSpec::Core::RakeTask.new(:spec, :tag) do |t, task_args|
  #   t.rspec_opts = "--tag #{task_args[:tag]}"
  # end
  # task :rspec do
  #   # TODO: need to run with same seed to guarantee order?
  #   system("bundle exec rspec --order defined --format RspecUmlFormatter::Basic --dry-run --out tmp/spec_uml.txt --tag uml")
  # end
  RSpec::Core::RakeTask.new(:spec_uml) do |t, task_args|
    t.rspec_opts = "--order defined --format RspecUmlFormatter::Basic --dry-run --tag uml"
  end

  RSpec::Core::RakeTask.new(:spec_custom, :filters) do |t, task_args|
    p "--order defined --format RspecUmlFormatter::Basic --dry-run #{task_args[:filters]}"
    t.rspec_opts = "--order defined --format RspecUmlFormatter::Basic --dry-run --tag uml #{task_args[:filters]}"
  end

  task :generate_images, [:path, :open_images] do |t, task_args|
    # system("docker run -d -p 8080:8080 plantuml/plantuml-server:jetty")
    plantuml = File.expand_path(File.join('..', '..', '..', 'bin', 'plantuml.jar'), __FILE__)
    uml_path = File.join(ENV.fetch("UML_DIR", "docs/uml"), task_args[:path])

    traverse_dir = lambda do |start_path|
      p "Traversing #{start_path}"
      Dir.foreach start_path do |entry|
        path = File.join(start_path, entry)
        if entry == "." or entry == ".." || File.extname(entry) == ".png"
          next
        elsif File.directory?(path)
          traverse_dir[path]
        else
          p "Generating image for #{entry}"
          system("java -jar #{plantuml} #{path}")
          system("open #{File.dirname(path)}/#{File.basename(entry, ".txt")}.png") if task_args[:open_images]
        end
      end
    end

    traverse_dir[uml_path]
  end
end