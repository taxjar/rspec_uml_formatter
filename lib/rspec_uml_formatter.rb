require 'rspec_uml_formatter/version'

module RspecUmlFormatter
  autoload :Basic, 'rspec_uml_formatter/basic'
  autoload :Uml, 'rspec_uml_formatter/uml'
  autoload :Persisters, 'rspec_uml_formatter/persisters'

  require 'rspec_uml_formatter/tasks' if defined?(Rake.application)
end
