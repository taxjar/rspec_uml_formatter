# RspecUmlFormatter

This is a [RSpec formatter](https://relishapp.com/rspec/rspec-core/docs/formatters/custom-formatters) which generates a [PlantUML](http://plantuml.com/) formatted text file which can be converted into an image.

## Why

- Visual representation of tests make it easier to understand what's going on
    - Code reviews: you can compare old and new images due to changes in the code and better see what's changed as well as just have a visual representation of spec coverage.
    - Feedback loop between tests and the resulting graphs. If the graph is too hard to follow or reason about then it's likely the spec is hard, or even harder, to read.
    - Easier to follow logic and steps in order to spot potential bugs or missing test cases.
- Documentation
    - Easier for developers and non-developers alike to follow a visual representation of scenarios. The graphs can be shared in several formats. Feature/integration test based graphs would give everyone a good overview of how the app works. Unit test based graphs would give developers quick and good overview of how the internals work.
    - Guaranteed to be up to date since it's built off current tests and in an automated way so no extra work required.

Good ole Martin Fowler (I swear he has written about everything I think about) has a bunch of articles on [UML](https://martinfowler.com/tags/uml.html) with [this one](https://martinfowler.com/bliki/UmlSketchingTools.html) being a relevant starting point.

## How

We tap into RSpec's tree nature to build a tree of UML nodes then traverse the tree with a couple of special rules to produce a PlantUML compliant representation.

The way that tests are organized are such that for every `describe` and `context` (just aliases in RSpec) there are 1 or many possible routes we can take including nested `describe`/`context` until an `it` block is reached -- what can be considered the leaf node. The purpose then is to just translate that tree structure into PlantUML's language. For now the [Activy Diagram](http://plantuml.com/activity-diagram-beta) is used but there many options you can find on the site or in this [PDF][PlantUML PDF].

- Every `describe` block (aka node) is a group (aka partition). Starting from the top `describe` block. These are represented by a [Fork Node](lib/rspec_uml_formatter/uml/fork_node.rb).
- Every `context` node is meant to be a special type of Fork. I say "meant to be" since `describe` and `context` are just aliases and the information passed to the formatter isn't enough to identify which was used. So an extra metadata tag has to be set (it can technically be set on any block not just `context` ones): `:uml_condition`. Also less than ideal is that since metadata is inherited, you have to unset this tag on children (just the first generation ones) if you don't want to them to also be considered conditions: `uml_condition: false`. Conditions on the same level are grouped together as one "node" in a chain of if/else logic.
- Every `it` is represented as the leaf node that leads to termination.

Currently it creates a file per non-conditional block following [VCR's](https://github.com/vcr/vcr) structure. The parent file containing the spec level UML contains placeholders for the blocks that are split up into their own files.

If only there was a way to represent this visually! Take a look at the [spec files](spec) and the results from running `UML_DIR=docs/uml rake ruf:run` in the [docs](docs/uml) directory. Preview:

![preview](docs/uml/SampleSpec.png "preview")

## What

Mark the specs you want covered with `:uml` metadata to produce the UML txt file then use PlantUML to generate an image.

See [usage](#usage) section for more details.

PlantUML isn't the tool/library around but seems like the best fit. I've also seen lots of plugins, auto-generated based off code (usually geared towards .NET, VisualBasic, C++ etc.), other text based tools like https://www.umlet.com/ etc. See more here http://plantuml.com/external-links

## Future

Current implementation is just a brute forced POC attempt while trying to decipher our billing system and to ensure that any changes won't cause any issues and to be able to reason better about what did change as well as being able to communicate that clearly.

In short, there's lots of room for improvement in the code itself but also the implementation on a high level. There are also endless possibilities with a tool like this so feel free to get creative and don't feel the need to be limited by this specific implementation.

Some immediate improvement ideas include:

- For large images
    - There are options like scaling `scale 750 width` (see section 9.2 in [PDF][PlantUML PDF])
    - Splitting diagrams (sections 1.8, 3.27 in [PDF][PlantUML PDF]) -- Currently doesn't work for activity diagrams
    - See the [large samples](samples/large) for a real case using the `StripeAutofileBillingWorker` and the `UserEnforcement` specs from the [taxjar repo](https://bitbucket.org/taxjar/taxjar/src). By default the image is limited in size so had to allow a larger image size by running `PLANTUML_LIMIT_SIZE=16384 SKIP_FIXTURES=true rake ruf:run`
    - Something worth noting here is the feedback loop. A very large and hard to follow diagram can indicate that the spec should be re-organized. One thing I like about elixir tests is that since no nesting is allowed, it encourages having separate files that concentrate on specific features/cases rather than having 1 file that attempts to cover everything.
- This was built off unit tests using `describe`/`context` not `feature`/`scenario` feature type tests but can definitely be used for that with better support.
- The conditions metadata tag to indicate what should be conditional is less than ideal since you have to get around RSpec's inheritance model
-  Use PlantUML's image [generating server](https://github.com/plantuml/plantuml-server) which we can host ourselves (docker option seems most viable):
    - Save the .txt file(s) in /doc
    - Save the URL that would represent each file either alongside the file or some other mechanism.
    - For example, their [hosted site](http://www.plantuml.com/plantuml/uml/fO_1Re8X48JlFCKUzHNgnJSnA8C_AIncTp5jchvxNTgOvcyyD1uWC0FppUxLejXeDSn3o5rUmyiIbI9T8Lew4uzsXEocH_4S2uTB52lcflQ6GxS5SaZGavGZw2EKJ1jtQ3h4K4EYKsV3cstf9Z9jtg7R7s3gq1bbI-4p3KKUTHV0YUO3L8NVV9KSFdztkRCUng_Qhdecu09vifm7VS_wVDnwrZ-Wgr9af_q3utyVG-wwI_lRDbhz0W00) (supposedly doesn't save any data but uses regular HTTP, no TLS so wouldn't recommend using it for anything sensitive) shows how this can be used -- that link produces the [sample spec](samples) output. The host part `http://www.plantuml.com/plantuml/uml` would just be wherever we host the java app and the encoded string is just the encoded .txt file. From the encoded string, the server displays the source code as well as the resulting image which can be downloaded or shared in several formats. See more on [encoding](http://plantuml.com/text-encoding).

Further off ideas:

- DB -> UML: https://www.simple-talk.com/sql/sql-tools/automatically-creating-uml-database-diagrams-for-sql-server/
- http://plantuml.com/gantt-diagram: visual in gantt from task manager/tracker/calendar? Useful for Autofile?
- Generate specs FROM UML. The process should be reversible. Sometimes it can be hard to write tests before implementation because you don't yet know what it will look like but what if you describe what it will look like first using a visual representation? Also since you can do that by coding and not fiddling with flakey UI to build diagrams.
- There are ways to link tests, diagrams -- this can be useful if there are related components in separate tests but we want to show how they're related. Also for shared_contexts/examples.
- The [PlantUML server](http://plantuml.com/server) can also produce the source code from a given diagram image file! Basically instead of the encoded string, you pass the image URL and it gives you back the source code. Pretty neat!


See more inspiration here: http://plantuml.com/running, http://plantuml.com/external-links. Ex:

- https://github.com/jvantuyl/sublime_diagram_plugin
- https://marketplace.atlassian.com/apps/1215115/plantuml-for-confluence-cloud?hosting=cloud&tab=overview
- https://github.com/mikitex70/plantuml-markdown

Advanced features: http://plantuml.com/preprocessing

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rspec_uml_formatter'
```

**NOTE** while developing this I'm just pointing to my local directory `gem "rspec_uml_formatter", path: "../rspec_uml_formatter"` but you can use `gem "rspec_uml_formatter", git: "https://bitbucket.org/taxjar/rspec_uml_formatter.git"` like we do with TaxRules. I place this in the development group so I can run the rake task from my console.

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rspec_uml_formatter

## Usage

Many improvements can be made here but for now, some helpful [rake tasks](lib/rspec_uml_formatter/tasks.rb) show it can be used.

First we run rspec using the `--format` option passing in `RspecUmlFormatter::Basic` as well as the `--dry-run` option since we don't need to actually run the tests while limiting coverage to specs with the `uml` metadata tag by using `--tag uml`. The output directory can be set by setting the `UML_DIR=some/dir` environment variable.

Then to generate images we use the [PlantUML jar](http://plantuml.com/starting) to produce the image -- it will be placed in the same location as the txt file -- then open the file for viewing. See `generate_images` task. **Note:** this requires java.


## TODO: Everything below here is default output to be filled in later.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/rspec_uml_formatter. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the RspecUmlFormatter project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/rspec_uml_formatter/blob/master/CODE_OF_CONDUCT.md).

[PlantUML PDF]: http://plantuml.com/PlantUML_Language_Reference_Guide.pdf
