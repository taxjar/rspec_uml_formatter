@startuml
title SampleSpec/some context/some describe
start
partition some_describe {
if (deeper context) then (yes)
split
:does something useful;
end split
elseif (other deeper context) then (yes)
split
:does something useful;
end split
else
end
endif
}
end
@enduml
