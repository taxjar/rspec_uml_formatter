RSpec.describe "SampleSpec", :uml do
  it "has a version number" do
    expect(RspecUmlFormatter::VERSION).not_to be nil
  end

  context "some context", uml: { condition: true } do
    describe "some describe", uml: { condition: false } do
      context "deeper context", uml: { condition: true } do
        it "does something useful" do
          expect(false).to eq(true)
        end
      end

      context "other deeper context", uml: { condition: true } do
        it "does something useful" do
          expect(false).to eq(true)
        end
      end
    end
  end

  context "vs another context", uml: { condition: true } do
    it "does something else" do
    end
  end

  describe "some other describe" do
    context "some other nested context", uml: { condition: true } do
      it "does something useful" do
        expect(false).to eq(true)
      end
    end
  end
end
